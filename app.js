const express = require('express')
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config/database');


const app = express();
app.use(bodyParser.urlencoded({
    extended: false
}))

// CORS MW
app.use(cors());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser MW
app.use(bodyParser.json());
// Invalid Route
app.get('/', (req, res) => {
    res.send('Invalid Endpoint... ');
})

// Start Server process.env.PORT || 
app.listen(3000, () => {
    console.log('Server started...');
})