const db = require('../../config/database');
let User = {
    /**
     *  returns all user info 
     * @param username the username
     * @param password the user password 
     * @param res  express response object
     */
    login: (username, password, res) => {
        db.connect().connect(function (err) {
            if (err) throw err;
            con.query("SELECT * FROM users WHERE username=? AND password=?", [username, password], function (err, result, fields) {
                if (err) throw err;
                res.send(result);
            });
        });
    },
    addUser: (userInfo, res) => {
        db.connect().connect(function (err) {
            if (err) throw err;
            var sql = "INSERT INTO users (name, username, email, password, phone, address, level) VALUES (?)";
            con.query(sql, [
                userInfo
            ], function (err, result) {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
    deleteUser: (id, res) => {
        db.connect().connect(function (err) {
            if (err) throw err;
            var sql = "DELETE FROM users WHERE id = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
    updateUser: (newInfo, id, res) => {
        db.connect().connect(function (err) {
            if (err) throw err;
            var sql = "UPDATE users SET ? WHERE ?";
            con.query(sql, [newInfo, id], function (err, result) {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
    testUsersList: () => {
        db.connect().connect(function (err) {
            if (err) throw err;
            console.log("Connected!");
            var sql = "INSERT INTO users (name, address,username,email,password,phone) VALUES ?";
            var values = [
                ['John', 'Highway 71', 'john71', 'john71@gmail.com', '123456798', '123456789'],
                ['Peter', 'Lowstreet 4', 'peter4', 'peter4@gmail.com', '123456798', '123456789'],
                ['Amy', 'Apple st 652', 'Amy652', 'Amy652@gmail.com', '123456798', '123456789'],
                ['Hannah', 'Mountain 21', 'Hannah21', 'Hannah21@gmail.com', '123456798', '123456789'],
                ['Michael', 'Valley 345', 'Michael345', 'Michael345@gmail.com', '123456798', '123456789'],
                ['Sandy', 'Ocean blvd 2', 'Sandy2', 'Sandy2@gmail.com', '123456798', '123456789'],
                ['Betty', 'Green Grass 1', 'Betty1', 'Betty1@gmail.com', '123456798', '123456789'],
                ['Richard', 'Sky st 331', 'Richard331', 'Richard331@gmail.com', '123456798', '123456789'],
                ['Susan', 'One way 98', 'Susan98', 'Susan98@gmail.com', '123456798', '123456789'],
                ['Vicky', 'Yellow Garden 2', 'Vicky2', 'Vicky2@gmail.com', '123456798', '123456789'],
                ['Ben', 'Park Lane 38', 'Ben38', 'Ben38@gmail.com', '123456798', '123456789'],
                ['William', 'Central st 954', 'William954', 'William954@gmail.com', '123456798', '123456789'],
                ['Chuck', 'Main Road 989', 'Chuck989', 'Chuck989@gmail.com', '123456798', '123456789'],
                ['Viola', 'Sideway 1633', 'Viola1633', 'Viola1633@gmail.com', '123456798', '123456789'],
                ['administrator', 'Sideway 1633', 'admin', 'admin@gmail.com', '66506657', '0096266506657']
            ];
            con.query(sql, [values], function (err, result) {
                if (err) throw err;
                console.log("Number of records inserted: " + result.affectedRows);
            });
        });
    },
    userRoutes: (app) => {
        // login request
        app.post('/login/', (req, res) => {
            var username = req.body.username;
            var password = req.body.password;
            User.login(username, password, res);
        });
        // add new user request
        app.post('/add/user/', (req, res) => {
            var name = req.body.name;
            var username = req.body.username;
            var password = req.body.password;
            var email = req.body.email;
            var phone = req.body.phone;
            var address = req.body.address;
            var userInfo = [
                name,
                username,
                password,
                email,
                phone,
                address
            ]
            User.addUser(userInfo, res);
        });
        // delete Request
        app.delete('/delete/user/', (req, res) => {
            var id = req.body.id;
            User.deleteUser(id, res);
        });
        // update Request
        app.put('/update/user/:id', (req, res) => {
            User.updateUser(JSON.parse(req.body.newInfo), {
                id: req.params.id
            }, res);
        });
    }

};

module.exports = User;